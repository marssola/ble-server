#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QObject>
#include <QBluetoothLocalDevice>
#include <QBluetoothDeviceInfo>
#include <QLowEnergyAdvertisingData>
#include <QLowEnergyAdvertisingParameters>
#include <QLowEnergyCharacteristic>
#include <QLowEnergyCharacteristicData>
#include <QLowEnergyDescriptorData>
#include <QLowEnergyController>
#include <QLowEnergyService>
#include <QLowEnergyServiceData>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include "HtLogging/htlogging.h"

HtLogging htlogging;
QString application_name;

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    application_name = QCoreApplication::applicationName();

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("logging", &htlogging);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QBluetoothDeviceInfo deviceInfo;
    QBluetoothLocalDevice localDevice;

//    const quint16 AutomationIOService = 0x1815;
    const quint16 customService = 0xFFE0;
    const quint16 customCharacteristic = 0xFFE1;

    qInfo() << "Device name" << localDevice.name();
    qInfo() << "Custom Service" << customService;

    QLowEnergyAdvertisingData advertisingData;
    advertisingData.setDiscoverability(QLowEnergyAdvertisingData::DiscoverabilityGeneral);
    advertisingData.setIncludePowerLevel(true);
    advertisingData.setLocalName(localDevice.name());
    advertisingData.setServices(QList<QBluetoothUuid>() << QBluetoothUuid(customService));

    QJsonObject obj({
                        {"nickname", ""},
                        {"value", 0},
                        {"type", 0},
                    });
    QByteArray data = QJsonDocument(obj).toJson(QJsonDocument::Compact);

    QLowEnergyCharacteristicData charData;
    charData.setUuid(QBluetoothUuid(customCharacteristic));
    charData.setValue(data);
    charData.setProperties(QLowEnergyCharacteristic::Broadcasting
                           |QLowEnergyCharacteristic::Read
                           |QLowEnergyCharacteristic::Notify
                           |QLowEnergyCharacteristic::Write);
    const QLowEnergyDescriptorData clientConfig(QBluetoothUuid::ClientCharacteristicConfiguration,
                                                data);
    charData.addDescriptor(clientConfig);

    QLowEnergyServiceData serviceData;
    serviceData.setType(QLowEnergyServiceData::ServiceTypePrimary);
    serviceData.setUuid(QBluetoothUuid(customService));
    serviceData.addCharacteristic(charData);

    const QScopedPointer<QLowEnergyController> leController(QLowEnergyController::createPeripheral());
    QScopedPointer<QLowEnergyService> service(leController->addService(serviceData));
    leController->startAdvertising(QLowEnergyAdvertisingParameters(), advertisingData, advertisingData);

    QTimer dataSimulatorTimer;
    QObject::connect(&dataSimulatorTimer, &QTimer::timeout, [&service, &obj]() {
        obj["value"] = obj["value"].toDouble() + 1;
        qDebug() << obj;
        QLowEnergyCharacteristic characteristic = service->characteristic(QBluetoothUuid(customCharacteristic));
        Q_ASSERT(characteristic.isValid());
        service->writeCharacteristic(characteristic, QJsonDocument(obj).toJson(QJsonDocument::Compact));
    });
    dataSimulatorTimer.start(2000);

    QObject::connect(leController.data(), &QLowEnergyController::connected, [=]() {
        qDebug() << "New device connected";
    });

    QObject::connect(leController.data(), &QLowEnergyController::disconnected, [&leController, advertisingData, &service, serviceData]() {
        qDebug() << "Reconect";
        service.reset(leController->addService(serviceData));
        if (!service.isNull())
            leController->startAdvertising(QLowEnergyAdvertisingParameters(), advertisingData, advertisingData);
    });

    return app.exec();
}
