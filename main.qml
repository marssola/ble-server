import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Window 2.12

ApplicationWindow {
    id: window

    visible: true
    width: 400
    height: 600
    title: qsTr("Hello World")

    Connections {
        target: logging
        onLogChanged: {
            var line = (logging.log.type? logging.log.type : "")
            line += (logging.log.file? " [" + logging.log.file + "]" : "")
            line += (logging.log.function? " [" + logging.log.function + "]" : "")
            line += " " + logging.log.msg
            textArea.append(line);
        }
    }

    ScrollView {
        anchors.fill: parent
        clip: true

        TextArea {
            id: textArea
            readOnly: true
            width: window.width
            height: window.height
            wrapMode: TextEdit.WordWrap
        }
    }
}
